import pytest

from blog.models import Content, Page


def _create_test_content(**kwargs):
    params = {
        "title": "test_title",
    }
    params.update(kwargs)
    content = Content.objects.create(**params)
    return content


@pytest.fixture
def content_factory():

    def factory(**kwargs):
        content = _create_test_content(**kwargs)
        return content

    return factory


@pytest.fixture
def page_factory():

    def factory(**kwargs):
        page = Page.objects.create(**kwargs)

        if "page_content" not in kwargs:
            new_content = _create_test_content()
            page.page_content.add(new_content)
            page.save()
        return page

    return factory


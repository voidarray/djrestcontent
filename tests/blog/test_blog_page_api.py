import pytest
from django.urls import reverse
from rest_framework.status import HTTP_200_OK

from blog.serializers import PageSerializer


@pytest.mark.django_db
def test_page_list(api_client, page_factory):
    _ = page_factory()

    url = reverse("pages")
    resp = api_client.get(url)

    assert resp.status_code == HTTP_200_OK

    resp_json = resp.json()

    assert len(resp_json) > 0


@pytest.mark.django_db
def test_page_details(api_client, page_factory):
    page = page_factory()
    page_ser = PageSerializer(page)

    url = reverse("page-full", kwargs={'pk': page.id})
    resp = api_client.get(url)

    assert resp.status_code == HTTP_200_OK

    resp_json = resp.json()

    assert resp_json == page_ser.data


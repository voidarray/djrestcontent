from django.db.models import F
from celery import shared_task

from .models import Content


@shared_task
def incr_views_counter(content_ids):
    Content.objects.filter(id__in=content_ids).update(view_count=F('view_count') + 1)

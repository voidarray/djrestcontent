import random

from django.core.management.base import BaseCommand, CommandError
from blog.models import Content, Page


class Command(BaseCommand):
    help = "Add test content"

    titles = [
        "The Colorful History of Cats",
        "A Guide to Cats at Any Age",
        "The Future According to Cats Experts",
        "Forget Everything You’ve Ever Known About Cats",
        "he 5 Best Cats Books of 2021",
        "How Cats Works",
        "What the Future of Cats Work Looks Like After Coronavirus",
        "Cats on a Budget: Our Best Money-Saving Tips",
        "The Piece of Cats Advice That’s Seared Into My Memory",
        "Cats Changed My Life. Here’s My Story",
    ]

    video_urls = [
        "https://youtu.be/dQw4w9WgXcQ",
        "http://tinyurl.com/349ygu",
    ]

    sub_urls = [
        "sub url 1",
        "sub url 2",
        "sub url 3",
    ]

    texts = [
        """Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. 
        Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века.""",
        """Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.""",
        """Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
        Excepteur sint occaecat cupidatat non proident, 
        sunt in culpa qui officia deserunt mollit anim id est laborum.""",
        """Mauris blandit, orci id cursus congue, mauris nulla rutrum metus, sed gravida lectus mauris id purus. 
        Curabitur consequat nec elit a rhoncus. Quisque ornare fermentum rhoncus.""",
        """Integer ac nisi eu quam cursus vestibulum at at nulla. Suspendisse non nisl enim. 
        Morbi porta convallis gravida. Ut ultrices imperdiet tortor. Mauris posuere neque at elementum viverra.""",
    ]

    def handle(self, *args, **options):
        for _ in range(30):
            choice = random.randint(1, 3)
            if choice == 1:
                Content.objects.create(
                    title=random.choice(self.titles),
                    type=Content.TYPE_VIDEO,
                    special={'video_url': random.choice(self.video_urls)},
                )
            elif choice == 2:
                Content.objects.create(
                    title=random.choice(self.titles),
                    type=Content.TYPE_AUDIO,
                    special={'audio_url': random.choice(self.video_urls)},
                )
            else:
                Content.objects.create(
                    title=random.choice(self.titles),
                    type=Content.TYPE_TEXT,
                    special={'text': random.choice(self.texts)},
                )

        for _ in range(16):
            new_page = Page()
            new_page.save()
            for _ in range(8):
                choice = random.randint(0, len(Content.TYPES))
                if choice > 0:
                    if choice == 1:
                        for_add = Content.objects.filter(type=Content.TYPE_VIDEO).order_by('?').first()
                    elif choice == 2:
                        for_add = Content.objects.filter(type=Content.TYPE_AUDIO).order_by('?').first()
                    else:
                        for_add = Content.objects.filter(type=Content.TYPE_TEXT).order_by('?').first()
                    new_page.page_content.add(for_add)
            new_page.save()

        print('Done!')

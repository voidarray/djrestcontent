from django.core.management.base import BaseCommand, CommandError
from blog.models import Content, Page


class Command(BaseCommand):
    help = "Drop all content"

    def handle(self, *args, **options):
        Content.objects.all().delete()
        Page.objects.all().delete()

        print('Content deleted!')

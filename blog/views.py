from django.http import Http404

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework import status

from .models import Page, Content
from .serializers import PageListSerializer, PageSerializer, ContentFullSerializer
from .tasks import incr_views_counter


class PageListPagination(PageNumberPagination):
    page_size = 3
    max_page_size = 100
    page_size_query_param = 'size'


class PageListView(ListAPIView):
    queryset = Page.objects.order_by('id').all()
    serializer_class = PageListSerializer
    pagination_class = PageListPagination


class PageView(APIView):

    def get_object(self, pk):
        try:
            obj = Page.objects.get(pk=pk)
        except Page.DoesNotExist:
            raise Http404

        content_ids_for_incr = list(obj.page_content.values_list('id', flat=True))
        incr_views_counter.delay(content_ids_for_incr)
        return obj

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PageSerializer(snippet)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PageSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


from django.db import models


class Content(models.Model):
    TYPE_VIDEO = 'video'
    TYPE_AUDIO = 'audio'
    TYPE_TEXT = 'text'

    TYPES = (
        (TYPE_VIDEO, 'Video'),
        (TYPE_AUDIO, 'Audio'),
        (TYPE_TEXT, 'Text'),
    )
    title = models.CharField(max_length=255)
    view_count = models.IntegerField(default=0)
    is_enable = models.BooleanField(default=True)
    type = models.CharField(max_length=10, choices=TYPES)
    special = models.JSONField(default=dict)

    def __str__(self):
        return f"{self.__class__.__name__} {self.title}"


class Page(models.Model):
    page_content = models.ManyToManyField(Content)

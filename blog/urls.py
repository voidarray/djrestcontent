from django.urls import path

from .views import PageListView, PageView

urlpatterns = [
    path("pages/", PageListView.as_view(), name="pages"),
    path("pages/<int:pk>/", PageView.as_view(), name="page-full"),
]

from django.contrib import admin
from django import forms
from django.db import models
from flat_json_widget.widgets import FlatJsonWidget

from .models import Content, Page


class ContentForm(forms.ModelForm):

    class Meta:
        model = Content
        exclude = ('id', )

    def clean(self):
        content_type = self.cleaned_data.get('type')
        special = self.cleaned_data.get('special', dict())
        missing_field = None
        if content_type == Content.TYPE_VIDEO:
            if 'video_url' not in special:
                missing_field = 'video_url'
        elif content_type == Content.TYPE_AUDIO:
            if 'audio_url' not in special:
                missing_field = 'audio_url'
        elif content_type == Content.TYPE_TEXT:
            if 'text' not in special:
                missing_field = 'text'

        if missing_field is not None:
            raise forms.ValidationError(f'There is no "{missing_field}" in special field')


class ContentAdmin(admin.ModelAdmin):
    search_fields = ['title', 'special']
    form = ContentForm
    formfield_overrides = {
        models.JSONField: {'widget': FlatJsonWidget},
    }


class ContentInlineAdmin(admin.StackedInline):
    model = Page.page_content.through


class PageAdmin(admin.ModelAdmin):
    inlines = [ContentInlineAdmin, ]


admin.site.register(Content, ContentAdmin)
admin.site.register(Page, PageAdmin)

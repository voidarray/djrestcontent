from django.urls import reverse
from rest_framework import serializers

from .models import Content, Page


class ContentFullSerializer(serializers.Serializer):
    title = serializers.CharField()
    view_count = serializers.IntegerField()

    class Meta:
        model = Content
        fields = ('title', 'view_count')

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        for k, v in instance.special.items():
            ret[k] = v
        return ret


class PageSerializer(serializers.Serializer):
    page_content = ContentFullSerializer(many=True)


class PageListSerializer(serializers.Serializer):
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        return reverse('page-full', kwargs={'pk': obj.id})

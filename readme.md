# What it is?

Test project with few models:

`Page` and `Content`

`Page` can contain different types of `Content`

# How to start?

`docker-compose up --build`

also, you can add test data in project via command:

`docker-compose exec api python manage.py add_test_content`

# How it use?

Start via docker and open 

to view pages

`http://localhost:8000/api/v1/pages/`

`http://localhost:8000/api/v1/pages/?page=2`

to view content on page

`http://localhost:8000/api/v1/pages/1/`

# Test

Pytest is using for tests

`docker-compose exec api pytest`

# Keywords

- Python 3.6 +
- Django 3
- Django Rest Framework
- Celery
- PostgreSQL
- Docker + docker‐compose

